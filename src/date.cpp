#include "date.hpp"
#include <sstream>

using namespace ribbon;

Date::Date(int year, int month, int day) :
    days(
        std::chrono::days(day)
        + std::chrono::duration_cast<std::chrono::days>(
            std::chrono::months(month))
        + std::chrono::duration_cast<std::chrono::days>(
            std::chrono::years(year)))
{
    std::stringstream ss;
    ss << year << "-" << month << "-" << day;
    printable = ss.str();
}
