#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <set>
#include <string>
#include <vector>

namespace ribbon
{
    namespace utils
    {
        class ReplaceTuple
        {
        public:
            ReplaceTuple(std::string search, std::string replace="");
            void apply(std::string &str) const;

            static void apply_all(
                std::string &str, const std::vector<ReplaceTuple> &tuples);

        private:
            std::string search;
            std::string replace;
        };

        std::set<int> parse_digits(const std::string &title);
        int parse_vndb_id(const std::string &str);

        bool string_contains(
            const std::string &str, const std::string &search);

        std::vector<std::string> split(
            const std::string &str, char split_char);

        std::string clean_output(const std::string &str);

        std::string replace_all(
            const std::string &str,
            const std::string &search,
            const std::string &replace);
        void replace_all_in_place(
            std::string &str,
            const std::string &search,
            const std::string &replace);
    }
}

#endif //STRING_UTILS_H
