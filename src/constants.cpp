#include "constants.hpp"

using namespace ribbon;

namespace ribbon::consts
{
    const std::string output_path = "output.txt";
    const std::string external_json_path = "../data/egs_releases.json";
    const std::string vndb_external_path = "../data/Releases with egs 2022-05-27.csv";
    const std::string vndb_without_external_json_path = "../data/Releases without egs 2022-05-27.json";

    const float ratio_min_value = 0.9;
    const float ratio_min_value_without_more_checks = 0.9;
    const int max_opcodes = 6;
    const std::chrono::days release_date_acceptable_diff = std::chrono::days(365);
    const std::chrono::days release_date_small_diff = std::chrono::days(60);
    const Date egs_tba_date = Date(2030, 1, 1);

    const std::set<int> excluded_vndb_vn_ids = {
        315, // Words Worth
        746, 747, 10515, 20493, 3190, 24563, 29320, 29321, 34143, 32206, 29424, 31391, 32500, // Apathy series
        802, 1347, 4670, 10951, 12171, 14340, // kuni no alice
        982, // happy breeding imasara fandisc
        2166, 2167, 2168, 2169, 2170, 8799, 16454, 21024, 28235, 28833, // Angelique series
        3381, // break panic
        4805, // Love Fetish ~Tech Gian Hen~ compilation
        4836, 8589, 4835, 8600, 8999, // Cal series
        6722, // そしてこの宇宙にきらめく君の詩 xxx
        7415, // teripura
        8158, 12480, 12600, // daitoshokan
        8413, 8414, 8415, 9392, 9393, 9784, // Rane Crisis
        9311, 9312, 9313, // hajimete no
        9432, // Harajuku After Dark
        9469, 14543, 21851, // Twilight Zone 1, 2, 4
        9480, // Fairie's Residence
        10054, // Hurtbreak Wonderland +
        11058, 11247, 11248, 11249, 11250, // Canon series
        11029, 11858, 14143, 19786, 19787, // noesis
        11124, 11125, 11126, 11127, 11128, 11130, // ねこねこソフト　おかえしＣＤ
        14164, // Melancholic dream tour
        15070, 20117, // birthday song series
        15997, // Issho ni Massage♪
        16666, // Ryuu no Hanayome wa Otoko no Ko
        17173, // kokuhaku
        17972, // Tsuushinbo ~Mama ni mo Naisho no Jikanwari~
        18543, 18544, 18545, 19867, // Kyokujitsu ni Kokoro
        18697, // Fukushuuya
        19661, // Tsumamigui 3 Spin-off
        20827, // Niku Denki II
        20914, // Saimin Kareshi Plus
        20980, // karakara2
        21964, 23725, // memories off innocent fille
        22055, // Fault Milestone Two Ge (Side: Below)
        23067, 25690, 26376, // Loca Love
        23409, // LOST:SMILE memories + promises
        23625, // Himekishi Ashley Inmon Monogatari
        27003, // Sadistic Gamers Part 3
        27081, // Karen
        27237, // yume no owari
        22492, 22493, // venus blood garden
        27454, // Kuro
        27587, 27586, // Osananajimi
        28542, // Shin Fushigi no Kuni no Alice
        29221, // Rakujitsu
        30763, // melty blood type lumina
        // My pet series
        9339, 9340, 9341, 9342, 9343, 9344, 18522, 18523, 18524, 18525,
        18508, 18509, 18510, 18511, 18512, 18513, 18514, 18515, 18516,
        18517, 18518, 18519, 18520, 18521, 18527, 18528, 18529, 18530,
        18531, 18532, 18534, 18535,
    };
}
