#ifndef STAT_DISPLAYER_H
#define STAT_DISPLAYER_H

#include <iostream>
#include <sstream>
#include <omp.h>

namespace ribbon
{
    class StatDisplayer
    {
    public:
        StatDisplayer(std::string text, size_t total) :
            text(text),
            total(total)
        {
        }

        void next()
        {
            #pragma omp atomic
            ++i;
        }

        void display()
        {
            auto percentage = static_cast<int>(
                static_cast<double>(i) / total * 100);
            if (percentage <= last_percentage)
                return;
            std::stringstream str;
            str << "\r" << text << " " << i << "/" << total
                << " " << percentage << "%\r";
            #pragma omp critical
            {
                last_percentage = percentage;
                std::cout << str.str() << std::flush;
            }
        }

        void end() const
        {
            std::cout << '\n';
        }

    private:
        std::string text;
        size_t i = 1;
        size_t total;
        int last_percentage = 0;
    };
}

#endif //STAT_DISPLAYER_H
