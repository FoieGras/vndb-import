#ifndef RELEASE_LINKER_H
#define RELEASE_LINKER_H

#include "link.hpp"

namespace ribbon
{
    class ReleaseLinker
    {
    public:
        ReleaseLinker(
            const std::vector<vndb::VndbRelease> &vndb_releases,
            const std::vector<ExternalRelease> &external_releases);

        std::vector<Link> link();

    private:
        bool should_ignore(const vndb::VndbRelease &vndb_release) const;
        void link_vndb_release(const vndb::VndbRelease &vndb_release);
        
        const std::vector<vndb::VndbRelease> &vndb_releases;
        const std::vector<ExternalRelease> &external_releases;
        std::vector<Link> validated_links;
    };
}

#endif //RELEASE_LINKER_H
