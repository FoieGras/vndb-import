#ifndef DATE_H
#define DATE_H

#include <string>
#include <chrono>

namespace ribbon
{
    class Date
    {
    public:
        Date(int year = 0, int month = 0, int day = 0);

        std::chrono::days days;
        std::string printable;
    };
}

#endif //DATE_H
