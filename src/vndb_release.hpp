#ifndef VNDB_RELEASE_H
#define VNDB_RELEASE_H

#include <set>
#include <string>
#include <vector>
#include "date.hpp"

namespace ribbon
{
    namespace vndb
    {
        class VndbRelease
        {
        public:
            int release_id = 0;
            int vn_id = 0;
            int egs_id = 0;
            Date release_date;
            std::string best_title;
            std::string title;
            std::string original_title;
            std::string vn_title;
            std::string vn_original_title;
            std::vector<std::string> aliases;
            std::string platform;
            std::string arranged_best_title;
            std::string arranged_title;
            std::string arranged_original_title;
            std::string arranged_vn_title;
            std::string arranged_vn_original_title;
            std::set<std::string> producer_names;
            std::string producer;
            std::set<int> digits;

            static std::vector<VndbRelease> load(const std::string &path);
            std::string to_string() const;
        };
    }
}

#endif //VNDB_RELEASE_H
