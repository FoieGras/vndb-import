#ifndef SIMPLE_LINK_H
#define SIMPLE_LINK_H

#include <string>
#include <vector>

namespace ribbon
{
    class SimpleLink
    {
    public:
        int external_id;
        int vndb_release_id;
        
        static std::vector<SimpleLink> load(const std::string &path);
    };
}

#endif //SIMPLE_LINK_H
