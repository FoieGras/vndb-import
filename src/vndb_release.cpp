#include "vndb_release.hpp"
#include <fstream>
#include <sstream>
#include <nlohmann/json.hpp>
#include <omp.h>
#include "constants.hpp"
#include "stat_displayer.hpp"
#include "string_arranger.hpp"
#include "string_utils.hpp"

using namespace ribbon;
using namespace ribbon::vndb;

static VndbRelease vndb_release_from_json(const nlohmann::json &entry);
static Date parse_vndb_date(const nlohmann::json &value);
static std::set<std::string> collect_vndb_producer_names(const nlohmann::json &entry);
static std::string ensure_string(const nlohmann::json &entry, const std::string &key);

std::vector<VndbRelease> VndbRelease::load(const std::string &path)
{
    std::ifstream vndb_file(path);
    nlohmann::json json_data;
    vndb_file >> json_data;

    std::vector<nlohmann::json> entries = json_data;
    StatDisplayer stat("Loading vndb releases", entries.size());

    std::vector<VndbRelease> releases;
    #pragma omp parallel for
    for (const auto &entry : entries)
    {
        stat.display();
        stat.next();

        auto release = vndb_release_from_json(entry);
        if (utils::string_contains(release.title, "DVD-ROM"))
            continue;

        #pragma omp critical
        releases.emplace_back(std::move(release));
    }
    stat.end();
    return releases;
}

static VndbRelease vndb_release_from_json(const nlohmann::json &entry)
{
    VndbRelease release;
    release.release_id = utils::parse_vndb_id(entry["id"]);
    release.vn_id = utils::parse_vndb_id(entry["vid"]);
    release.egs_id = entry.value("l_egs", 0);
    release.title = entry["title"];
    release.original_title = entry["original"];
    release.vn_title = entry["vn_title"];
    release.vn_original_title = entry["vn_original"];
    release.aliases = utils::split(entry["alias"], '\n');
    if (entry["platform"].type() == nlohmann::json::value_t::string)
        release.platform = arrange::platform(entry["platform"]);
    release.release_date = parse_vndb_date(entry["released"]);
    release.producer_names = collect_vndb_producer_names(entry);

    release.best_title = (
        release.original_title.empty() ? release.title : release.original_title
    );
    release.arranged_best_title = arrange::title(release.best_title);
    release.arranged_title = arrange::title(release.title);
    release.arranged_original_title = arrange::title(release.original_title);
    release.arranged_vn_title = arrange::title(release.vn_title);
    release.arranged_vn_original_title = arrange::title(release.vn_original_title);
    release.digits = utils::parse_digits(release.arranged_best_title);
    return release;
}

static Date parse_vndb_date(const nlohmann::json &value)
{
    if (value.type() == nlohmann::json::value_t::string)
        return consts::egs_tba_date;
    int date = value;
    auto year = date / 10000;
    auto month = (date / 100) % 100;
    auto day = date % 100;
    if (day == 99 || month == 99)
        return consts::egs_tba_date;
    return Date(year, month, day);
}

static std::set<std::string> collect_vndb_producer_names(const nlohmann::json &entry)
{
    static std::vector<std::string> keys = {
        "p_dev_name", "p_dev_original",
        "p_pub_name", "p_pub_original",
    };

    std::set<std::string> base_names;
    for (const auto &key : keys)
        base_names.insert(ensure_string(entry, key));

    auto dev_alias = ensure_string(entry, "p_dev_alias");
    auto dev_aliases = utils::split(dev_alias, '\n');
    base_names.insert(dev_aliases.begin(), dev_aliases.end());

    auto pub_alias = ensure_string(entry, "p_pub_alias");
    auto pub_aliases = utils::split(pub_alias, '\n');
    base_names.insert(pub_aliases.begin(), pub_aliases.end());

    std::set<std::string> arranged_names;
    for (const auto &name : base_names)
    {
        auto arranged_name = arrange::producer(name);
        if (!arranged_name.empty())
            arranged_names.insert(arranged_name);
    }
    return arranged_names;
}

static std::string ensure_string(const nlohmann::json &entry, const std::string &key)
{
    if (!entry.contains(key))
        return "";

    nlohmann::json value = entry[key];
    if (value.type() == nlohmann::json::value_t::null)
        return "";
    return value;
}

std::string VndbRelease::to_string() const
{
    std::stringstream str;
    str << "VndbRelease '" << release_id << "'"
        << " '" << title << "'"
        << " '" << platform << "'"
        << " '" << arranged_title << "'"
        ;
    return str.str();
}
