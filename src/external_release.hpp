#ifndef EXTERNAL_RELEASE_H
#define EXTERNAL_RELEASE_H

#include <set>
#include <string>
#include <vector>
#include "date.hpp"

namespace ribbon
{
    class ExternalRelease
    {
    public:
        // json
        int id = 0;
        std::string id_string;
        std::string title;
        std::string platform;
        Date release_date;
        std::set<std::string> producer_names;

        // computed
        std::string arranged_title;
        std::set<int> digits;

        static std::vector<ExternalRelease> load(const std::string &path);
        void compute_fields();
        std::string get_best_id() const;
        std::string to_string() const;
    };
}

#endif //EXTERNAL_RELEASE_H
