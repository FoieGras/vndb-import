#include "release_linker.hpp"
#include <algorithm>
#include <sstream>
#include <thread>
#include <omp.h>
#include "constants.hpp"
#include "stat_displayer.hpp"

using namespace ribbon;
using namespace ribbon::vndb;

static Link &get_best_ratio_link(std::vector<Link> &links);
static bool link_less(const Link &left, const Link &right);

ReleaseLinker::ReleaseLinker(
    const std::vector<VndbRelease> &vndb_releases,
    const std::vector<ExternalRelease> &external_releases
) :
    vndb_releases(vndb_releases),
    external_releases(external_releases)
{
}

std::vector<Link> ReleaseLinker::link()
{
    StatDisplayer stat("Linking", vndb_releases.size());

    #pragma omp parallel for
    for (const auto &vndb_release : vndb_releases)
    {
        stat.display();
        stat.next();

        if (should_ignore(vndb_release))
            continue;

        link_vndb_release(vndb_release);
    }
    stat.end();

    std::sort(validated_links.begin(), validated_links.end(), link_less);
    return std::move(validated_links);
}

bool ReleaseLinker::should_ignore(const VndbRelease &vndb_release) const
{
    return consts::excluded_vndb_vn_ids.contains(vndb_release.vn_id);
}

void ReleaseLinker::link_vndb_release(const VndbRelease &vndb_release)
{
    std::vector<Link> links;
    for (const auto &external_release : external_releases)
    {
        Link link(&vndb_release, &external_release);
        if (link.is_acceptable())
            links.push_back(link);
    }

    if (links.empty())
        return;

    for (auto &link : links)
        link.compute_ratios();
    auto &best_link = get_best_ratio_link(links);
    if (!best_link.is_ratio_acceptable())
        return;

    #pragma omp critical
    validated_links.emplace_back(best_link);
}

static Link &get_best_ratio_link(std::vector<Link> &links)
{
    auto &best_link = links.at(0);
    for (auto &link : links)
        if (link.ratio > best_link.ratio)
            best_link = link;
    return best_link;
}

static bool link_less(const Link &left, const Link &right)
{
    auto ratio_diff = left.ratio - right.ratio;
    if (ratio_diff != 0)
        return left.ratio < right.ratio;
    auto day_diff = left.get_day_diff() - right.get_day_diff();
    if (day_diff != 0)
        return left.get_day_diff() > right.get_day_diff();
    return left.external_release->id < right.external_release->id;
}
