#include <iostream>
#include <fstream>
#include <boost/locale.hpp>
#include <boost/stacktrace.hpp>
#include "constants.hpp"
#include "release_linker.hpp"
#include "simple_link.hpp"

using namespace ribbon;
using namespace ribbon::vndb;

static void set_locale();

static void link_vndb_releases();
static void write_output(const std::vector<Link> &links);
static void print_results(const std::vector<Link> &links);
static std::set<int> get_known_external_ids();
static std::set<int> get_new_external_ids(const std::vector<Link> &links);


int main()
{
    set_locale();
    try
    {
        link_vndb_releases();
        return EXIT_SUCCESS;
    }
    catch (const std::exception &e)
    {
        std::cerr
            << "Failure: "
            << e.what() << '\n'
            << boost::stacktrace::stacktrace() << '\n';
        return EXIT_FAILURE;
    }
}

static void set_locale()
{
    boost::locale::generator generator;
    std::locale locale = generator("");
    std::locale::global(locale);
    std::cout.imbue(locale);
}


static void link_vndb_releases()
{
    std::cout << "Loading external releases...\n";
    auto external_releases = ExternalRelease::load(consts::external_json_path);
    std::cout << "Loading vndb releases...\n";
    auto vndb_releases = VndbRelease::load(consts::vndb_without_external_json_path);

    std::cout
        << "Loaded:\n"
        << vndb_releases.size() << " vndb releases \n"
        << external_releases.size() << " external releases\n"
        ;

    ReleaseLinker linker(vndb_releases, external_releases);
    auto links = linker.link();

    write_output(links);
    print_results(links);
}

static void write_output(const std::vector<Link> &links)
{
    std::cout << "Writting output...\n";
    std::ofstream output_file(consts::output_path);
    for (const auto &link : links)
        output_file << link.to_output_string() << '\n';
}

static void print_results(const std::vector<Link> &links)
{
    auto known_external_ids = get_known_external_ids();
    auto new_external_ids = get_new_external_ids(links);
    std::set<int> new_unknown_external_ids;
    std::set_difference(
        new_external_ids.begin(), new_external_ids.end(),
        known_external_ids.begin(), known_external_ids.end(),
        std::inserter(new_unknown_external_ids, new_unknown_external_ids.begin())
    );

    std::cout
        << "Done!\n"
        << links.size() << " matches\n"
        << new_unknown_external_ids.size() << " new external ids\n"
        ;
}

static std::set<int> get_known_external_ids()
{
    auto simple_links = SimpleLink::load(consts::vndb_external_path);
    std::set<int> external_ids;
    for (const auto &simple_link : simple_links)
        external_ids.insert(simple_link.external_id);
    return external_ids;
}

static std::set<int> get_new_external_ids(const std::vector<Link> &links)
{
    std::set<int> external_ids;
    for (const auto &link : links)
        external_ids.insert(link.external_release->id);
    return external_ids;
}
