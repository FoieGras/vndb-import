#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <chrono>
#include <set>
#include <string>
#include "date.hpp"

namespace ribbon
{
    namespace consts
    {
        extern const std::string output_path;
        extern const std::string external_json_path;
        extern const std::string vndb_external_path;
        extern const std::string vndb_without_external_json_path;
        extern const float ratio_min_value;
        extern const float ratio_min_value_without_more_checks;
        extern const int max_opcodes;
        extern const std::chrono::days release_date_acceptable_diff;
        extern const std::chrono::days release_date_small_diff;
        extern const Date egs_tba_date;

        extern const std::set<int> excluded_vndb_vn_ids;
    }
}

#endif //CONSTANTS_H
