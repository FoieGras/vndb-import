#include "string_arranger.hpp"
#include "string_utils.hpp"
#include <map>
#include <boost/locale.hpp>

using namespace ribbon;

static const std::vector<utils::ReplaceTuple> title_replace =
{
    { "初回超重量版" },
    { "初回限定版" },
    { "初回版" },
    { "通常版" },
    { "限定版" },
    { "完全生産限定版" },
    { "全年齢対象版" },
    { "ダウンロード版" },
    { "ダウンロード" },
    { "パッケージ版" },
    { "プレミアムエディション" },
    { "CD Edition" },
    { "FD Edition" },
    { "NA Edition" },
    { "First Press Limited Edition" },
    { "First Press Special Edition" },
    { "First Press Edition" },
    { "Deluxe Edition" },
    { "Download Edition" },
    { "Early Edition" },
    { "Limited Edition" },
    { "Package Edition" },
    { "Premium Edition" },
    { "Regular Edition" },
    { "Standard Edition" },
    { "DX Pack" },
    { "DXパック" },
    { "DL版" },
    { "FD版" },
    { "Android版" },
    { "Windows版" },
    { "CD-ROM版" },
    { "CDケース版" },
    { "for Windows" },
    { "for windows" },
    { "with SoundTrack" },
    { "(Win Version)" },
    { "(Win)" },
    { "(DOS)" },
    { "(Win95)" },
    { "PlayStation Vita" },
    { "Portable" }, // ?
};

static const std::vector<utils::ReplaceTuple> producer_replace =
{
    // prefix/suffix
    { "総合研究所" },
    { "有限会社" },
    { "株式会社" },
    { "電子産業" },
    { "スタジオ" },
    { "同人工房" },
    { "相討ち" },
    { "Corporation" },
    { "Entertainment" },
    { "SOFT" },
    { "Soft" },
    { "INC." },
    { "Co., Ltd." },
    // replace company names
    { "agony/禁飼育", "禁飼育" },
    { "モーニングスター", "じぃすぽっと"},
    { "Xing Entertainment", "Xing" },
    { "6045* -Roku Maru Yon Go-", "6045*" },
    { "5pb. Games", "5pb." },
    { "MAGES.", "MAGES. GAME" },
    { "縁 -yukari-", "縁" },
    { "CHARON×cream△", "CHARON" },
    { "ディーゼルマインMf", "ディーゼルマイン" },
    { "月うさぎProject", "月うさぎプロジェクト" },
    { "CHAOS-R feat. Freak Strike", "CHAOS-R" },
    { "FAIRYTALE X指定", "Fairytale X-Shitei" },
    { "STRIKES DL", "Strikes" },
    { "Interchannel、NECインターチャネル", "Interchannel" },
    { "田辺組 TEAM-TANABE", "Team-Tanabe"},
    { "(猫)milkcat", "milkcat" },
    { "叢神", "elle-叢神" },
    { "TR九石堂/System9", "TR九石堂" },
    { "F&C FC01", "F&C" },
    { "F&C FC02", "F&C" },
    { "神無月製作所・妻魅組", "神無月製作所" },
};

static const std::vector<utils::ReplaceTuple> symbols =
{
    // basic symbols
    { " " },
    { "." },
    { "\t" },
    { "?" }, { "!" },
    { "'" }, { "\"" },
    { "~" },
    { "+" }, { "-" }, { "*" }, { "/" },
    { ":" }, { ";" },
    // regular width
    { "…" },
    { "‘" }, { "’" },
    { "×" },
    { "ﾟ" },
    { "･" },
    { "●" }, { "♥" }, { "❤︎" }, { "♪" },
    { "■" }, { "□" },
    // large width
    { "　" }, { "‐" },
    { "："},
    { "・" }, { "、" }, { "．" }, { "。" },
    { "～" },
    { "゜" },
    { "？" }, { "！" },
    { "＋" }, { "－" }, { "＊" }, { "／" },
    { "★" }, { "☆" },
    { "⚫" },
    // brackets
    { "<" }, { ">" },
    { "[" }, { "]" },
    { "(" }, { ")" },
    { "「" },{ "」" },
    { "『" }, { "』" },
    { "≪" }, { "≫" },
    { "＜" }, { "＞" },
    { "〈" }, { "〉" },
    { "（" }, { "）" },
    { "〔" }, { "〕" },
};

static const std::vector<utils::ReplaceTuple> roman_numbers =
{
    { "XII", "12" },
    // { "XI", "11" },
    // { "X", "10" },
    // { "IX", "9" },
    { "VIII", "8" },
    { "VII", "7" },
    // { "VI", "6" },
    // { "V", "5" },
    // { "IV", "4" },
    { "IIII", "4" },
    { "III", "3" },
    // { "II", "2" },
    // { "I", "1" },
};

static const std::map<std::string, std::string> platform_mapping =
{
    // other
    { "web", "WEB" },
    // OS
    { "win", "PC" },
    { "lin", "PC" },
    { "mac", "PC" },
    // old computers
    { "dos", "DOS" },
    { "p88", "PC" },
    { "p98", "PC" },
    { "msx", "PC" },
    { "x1s", "PC" },
    { "x68", "PC" },
    { "fmt", "PC" },
    { "fm7", "PC" },
    { "fm8", "PC" },
    { "pce", "PCE" },
    // phone
    { "and", "Android" },
    { "ios", "iOS" },
    // console
    { "sat", "SS" },
    { "drc", "DC" },
    // playstation
    { "psp", "PSP" },
    { "psv", "PSV" },
    { "ps1", "PS" },
    { "ps2", "PS2" },
    { "ps3", "PS3" },
    { "ps4", "PS4" },
    { "ps5", "PS5" },
    // nintendo
    { "fmc", "FC" },
    { "sfc", "SFC" },
    { "nds", "NDS" },
    { "n3d", "3DS" },
    { "gbc", "GBA(GB)" },
    { "gba", "GBA(GB)" },
    { "wii", "Wii" },
    { "wiu", "Wii U" },
    // xbox
    { "xb1", "XB" },
    { "xb3", "XB360" },
    { "xbo", "XBO" },
};

std::string arrange::title(const std::string &title)
{
    std::string copy = title;
    utils::ReplaceTuple::apply_all(copy, title_replace);
    utils::ReplaceTuple::apply_all(copy, symbols);
    copy = boost::locale::normalize(copy, boost::locale::norm_nfkc);
    copy = boost::locale::to_lower(copy);
    return copy;
}

std::string arrange::producer(const std::string &name)
{
    std::string copy = name;
    utils::ReplaceTuple::apply_all(copy, producer_replace);
    utils::ReplaceTuple::apply_all(copy, symbols);
    copy = boost::locale::normalize(copy, boost::locale::norm_nfkc);
    copy = boost::locale::to_lower(copy);
    return copy;
}

std::string arrange::platform(const std::string &platform)
{
    if (platform_mapping.contains(platform))
        return platform_mapping.at(platform);
    return boost::locale::to_upper(platform);
}
