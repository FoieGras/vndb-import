#include "string_utils.hpp"
#include <regex>
#include <boost/algorithm/string.hpp>

using namespace ribbon;

utils::ReplaceTuple::ReplaceTuple(std::string search, std::string replace) :
    search(search),
    replace(replace)
{
}

void utils::ReplaceTuple::apply(std::string &str) const
{
    utils::replace_all_in_place(str, search, replace);
}

void utils::ReplaceTuple::apply_all(
    std::string &str, const std::vector<ReplaceTuple> &tuples)
{
    for (const auto &tuple : tuples)
        tuple.apply(str);
}

std::set<int> utils::parse_digits(const std::string &title)
{
    std::regex digit_regex("((0|1|2|3|4|6|5|7|8|9)+)");
    auto begin = std::sregex_iterator(title.begin(), title.end(), digit_regex);
    auto end = std::sregex_iterator();
    std::set<int> digits;
    for (auto digit = begin; digit != end; ++digit)
    {
        auto string = digit->str();
        if (string.size() < 6)
            digits.insert(std::stoi(string));
    }
    return digits;
}

int utils::parse_vndb_id(const std::string &str)
{
    return std::stoi(str.substr(1));
}

bool utils::string_contains(
    const std::string &str, const std::string &search)
{
    return str.find(search) != std::string::npos;
}

std::vector<std::string> utils::split(
    const std::string &str, char split_char)
{
    auto is_char = [split_char](char c) {
        return c == split_char;
    };
    std::vector<std::string> parts;
    boost::split(parts, str, is_char);
    return parts;
}

std::string utils::clean_output(const std::string &str)
{
    return utils::replace_all(str, "\t", "");
}

std::string utils::replace_all(
    const std::string &str, const std::string &search, const std::string &replace)
{
    std::string copy = str;
    replace_all_in_place(copy, search, replace);
    return copy;
}

void utils::replace_all_in_place(
    std::string &str, const std::string &search, const std::string &replace)
{
    size_t position = 0;
    while (true)
    {
        position = str.find(search, position);
        if (position == std::string::npos)
            break;
        str.replace(position, search.length(), replace);
        position += replace.length();
    }
}
