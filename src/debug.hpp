#ifndef DEBUG_H
#define DEBUG_H

#include <iostream>

#define DEBUG(str)                                      \
    do {                                                \
        std::cerr                                       \
            << "-- Debug "                              \
            << __FILE__ << " l" << __LINE__ << " "      \
            << __FUNCTION__ << ":\t"                    \
            << str                                      \
            << '\n';                                    \
    } while (0)

#endif //DEBUG_H
