#ifndef LINK_H
#define LINK_H

#include "external_release.hpp"
#include "vndb_release.hpp"

namespace ribbon
{
    enum class RatioType
    {
        none = 0,
        title,
        vn_title,
        original_title,
        vn_original_title,
        alias
    };

    class Link
    {
    public:
        Link() = default;
        Link(const vndb::VndbRelease *vndb_release,
             const ExternalRelease *external_release);

        bool has_same_platform() const;
        bool has_common_producer() const;
        bool has_same_digits() const;
        int get_day_diff() const;
        bool has_small_release_date_diff() const;
        bool has_acceptable_release_date_diff() const;

        bool is_acceptable() const;
        bool is_ratio_acceptable() const;
        void compute_ratios();

        std::string to_output_string() const;

        const vndb::VndbRelease *vndb_release = nullptr;
        const ExternalRelease *external_release = nullptr;
        double ratio = 0;
        RatioType ratio_type = RatioType::none;

    private:
        void check_ratio(const std::string &vndb_title, RatioType current_type);
    };
}

#endif //LINK_H
