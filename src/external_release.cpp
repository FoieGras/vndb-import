#include "external_release.hpp"
#include <fstream>
#include <sstream>
#include <nlohmann/json.hpp>
#include <omp.h>
#include "stat_displayer.hpp"
#include "string_arranger.hpp"
#include "string_utils.hpp"

using namespace ribbon;

static ExternalRelease external_release_from_json(const nlohmann::json &entry);
static Date parse_release_date(const std::string &str);
static std::set<std::string> parse_producer_names(const std::vector<nlohmann::json> &names);

std::vector<ExternalRelease> ExternalRelease::load(const std::string &path)
{
    std::ifstream input_file(path);
    nlohmann::json json_data;
    input_file >> json_data;
    
    std::vector<nlohmann::json> entries = json_data;
    StatDisplayer stat("Loading external releases", entries.size());

    std::vector<ExternalRelease> releases;
    #pragma omp parallel for
    for (const auto &entry : entries)
    {
        stat.display();
        stat.next();

        auto release = external_release_from_json(entry);
        if (release.arranged_title == "")
            continue;

        #pragma omp critical
        releases.emplace_back(std::move(release));
    }
    stat.end();
    return releases;
}


static ExternalRelease external_release_from_json(const nlohmann::json &entry)
{
    ExternalRelease release;
    nlohmann::json value = entry["id"];
    if (value.type() != nlohmann::json::value_t::string)
        release.id = value;
    else
        release.id_string = value;
    release.title = entry["title"];
    release.platform = entry["platform"];
    release.release_date = parse_release_date(entry["release_date"]);
    release.producer_names = parse_producer_names(entry["producer_names"]);

    release.compute_fields();
    return release;
}

static Date parse_release_date(const std::string &str)
{
    auto parts = utils::split(str, '-');
    return Date(
        std::stoi(parts.at(0)),
        std::stoi(parts.at(1)),
        std::stoi(parts.at(2))
    );
}

static std::set<std::string> parse_producer_names(const std::vector<nlohmann::json> &names)
{
    std::set<std::string> producer_names;
    for (const auto &name : names)
    {
        std::string producer_name = arrange::producer(name);
        producer_names.insert(producer_name);
    }
    return producer_names;
}

void ExternalRelease::compute_fields()
{
    arranged_title = arrange::title(title);
    digits = utils::parse_digits(arranged_title);
}

std::string ExternalRelease::get_best_id() const
{
    if (!id_string.empty())
        return id_string;
    std::stringstream str;
    str << id;
    return str.str();
}

std::string ExternalRelease::to_string() const
{
    std::stringstream str;
    str << "ExternalRelease '" << get_best_id() << "'"
        << " '" << title << "'"
        << " '" << arranged_title << "'"
        ;
    return str.str();
}

