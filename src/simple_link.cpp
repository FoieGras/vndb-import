#include "simple_link.hpp"
#include <fstream>
#include "string_utils.hpp"

using namespace ribbon;

static SimpleLink simple_link_from_line(const std::string &line);

std::vector<SimpleLink> SimpleLink::load(const std::string &path)
{
    std::ifstream file(path);

    std::string header;
    getline(file, header);
    (void) header;

    std::vector<SimpleLink> links;
    std::string line;
    while (getline(file, line))
    {
        auto link = simple_link_from_line(line);
        links.emplace_back(link);
    }

    return links;
}

static SimpleLink simple_link_from_line(const std::string &line)
{
    auto parts = utils::split(line, ',');
    SimpleLink link;
    link.vndb_release_id = utils::parse_vndb_id(parts.at(0));
    link.external_id = std::stoi(parts.at(1));
    return link;
}
