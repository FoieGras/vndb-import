#ifndef STRING_ARRANGER_H
#define STRING_ARRANGER_H

#include <string>

namespace ribbon
{
    namespace arrange
    {
        std::string title(const std::string &title);
        std::string producer(const std::string &name);
        std::string platform(const std::string &platform);
    }
}

#endif //STRING_ARRANGER_H
