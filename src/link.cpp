#include "link.hpp"
#include <sstream>
#include "constants.hpp"
#include "difflib/SequenceMatcher.h"
#include "string_utils.hpp"

using namespace ribbon;
using namespace ribbon::vndb;

static std::map<RatioType, std::string> ratio_type_names =
{
    { RatioType::none, "none" },
    { RatioType::title, "title" },
    { RatioType::vn_title, "vn_title" },
    { RatioType::original_title, "original" },
    { RatioType::vn_original_title, "vn_original" },
    { RatioType::alias, "alias" },
};

static int count_opcode_diff(const std::vector<difflib::Opcode> &opcodes);


Link::Link(const VndbRelease *vndb_release, const ExternalRelease *external_release) :
    vndb_release(vndb_release),
    external_release(external_release)
{
}

bool Link::has_same_platform() const
{
    return vndb_release->platform == external_release->platform;
}

bool Link::has_common_producer() const
{
    for (const auto &producer : vndb_release->producer_names)
        if (external_release->producer_names.contains(producer))
            return true;
    return false;
}

bool Link::has_same_digits() const
{
    return vndb_release->digits == external_release->digits;
}

int Link::get_day_diff() const
{
    return std::abs(
        vndb_release->release_date.days.count() -
        external_release->release_date.days.count()
    );
}

bool Link::has_small_release_date_diff() const
{
    return get_day_diff() < consts::release_date_small_diff.count();
}

bool Link::has_acceptable_release_date_diff() const
{
    return get_day_diff() < consts::release_date_acceptable_diff.count();
}

bool Link::is_acceptable() const
{
    return has_same_platform()
        && has_common_producer()
        && has_same_digits()
        && has_acceptable_release_date_diff()
        ;
}

bool Link::is_ratio_acceptable() const
{
    if (ratio >= consts::ratio_min_value_without_more_checks)
        return true;
    if (ratio >= consts::ratio_min_value)
        return get_day_diff() <= consts::release_date_small_diff.count();
    return false;
}

void Link::compute_ratios()
{
    check_ratio(vndb_release->arranged_title, RatioType::title);
    check_ratio(vndb_release->arranged_original_title, RatioType::original_title);
    for (const auto &alias : vndb_release->aliases)
        check_ratio(alias, RatioType::alias);
    check_ratio(vndb_release->arranged_vn_title, RatioType::vn_title);
    check_ratio(vndb_release->arranged_vn_original_title, RatioType::vn_original_title);
}

void Link::check_ratio(const std::string &vndb_title, RatioType current_type)
{
    difflib::SequenceMatcher matcher(vndb_title, external_release->arranged_title);
    double current_ratio = matcher.RealQuickRatio();
    if (current_ratio < consts::ratio_min_value || current_ratio <= ratio)
        return;
    current_ratio = matcher.QuickRatio();
    if (current_ratio < consts::ratio_min_value || current_ratio <= ratio)
        return;
    current_ratio = matcher.Ratio();
    if (current_ratio < consts::ratio_min_value || current_ratio <= ratio)
        return;

    auto opcodes = matcher.GetOpcodes();
    if (count_opcode_diff(opcodes) > consts::max_opcodes)
        return;

    ratio = current_ratio;
    ratio_type = current_type;
}

static int count_opcode_diff(const std::vector<difflib::Opcode> &opcodes)
{
    int opcode_diff_count = 0;
    for (const auto &opcode : opcodes)
        if (opcode.tag != "equal")
            opcode_diff_count++;
    return opcode_diff_count;
}

std::string Link::to_output_string() const
{
    std::stringstream str;
    str << external_release->get_best_id() << "\t"
        << utils::clean_output(external_release->title) << "\t"
        << "r" << vndb_release->release_id << "\t"
        << "v" << vndb_release->vn_id << "\t"
        << utils::clean_output(vndb_release->best_title) << "\t"
        << utils::clean_output(vndb_release->platform) << "\t"
        << ratio << "\t"
        << ratio_type_names.at(ratio_type) << "\t"
        << get_day_diff() << "d"
        ;
    return str.str();
}
