# VNDB import
Bunch of stuff for importing links into vndb. Everything is under GPLv3, except a few files where another readme indicates otherwise.

## VNDB egs
Link vndb and egs releases.

Dependencies:
- boost
- nlohmann/json

## Getting input data
### VNDB releases without EGS
https://query.vndb.org/queries/2c8ca267-23af-4592-90e3-47b1b9e8b545
Download as json

### VNDB releases with EGS
https://query.vndb.org/queries/5f6f5fd6-f54d-43c6-b2dc-5a651f65c0fe
Download as csv

### EGS releases
http://erogamescape.dyndns.org/~ap2/ero/toukei_kaiseki/sql_for_erogamer_form.php
```
select g.id, g.gamename, g.sellday, g.model, b.brandname
from gamelist g
inner join brandlist b on b.id = g.brandname
order by g.id;
```
Download the web page

### VNDB releases without EGS (query copy)
https://query.vndb.org/queries/2c8ca267-23af-4592-90e3-47b1b9e8b545
```
with simple_release(id, vid, platform, pid_dev, pid_pub) as (
  select r.id, rv.vid, (
      select rp.platform
      from releases_platforms rp
      where rp.id = r.id
      order by rp.platform desc
      limit 1
    ) "platform", (
      select rp.pid
      from releases_producers rp
      where rp.id = r.id
        and rp.developer = true
      order by rp.pid
      limit 1
    ) "pid_dev", (
      select rp.pid
      from releases_producers rp
      where rp.id = r.id
        and rp.publisher = true
      order by rp.pid
      limit 1
    ) "pid_pub"
  from releases r
  inner join releases_vn rv on rv.id = r.id
  where r.id in ( -- no multiple vn for this release
      select releases_vn.id
      from releases_vn
      where releases_vn.id = r.id
      group by releases_vn.id
      having count(*) = 1
    )
)
select r.id, sr.vid,
  r.title, r.original,
  r.released, sr.platform,
  vn.title "vn_title", vn.original "vn_original", vn.alias,
  p_dev.original "p_dev_original", p_dev.name "p_dev_name", p_dev.alias "p_dev_alias",
  p_pub.original "p_pub_original", p_pub.name "p_pub_name", p_pub.alias "p_pub_alias"
from simple_release sr
inner join releases r on r.id = sr.id
inner join vn on vn.id = sr.vid
left join producers p_dev on p_dev.id = sr.pid_dev
left join producers p_pub on p_pub.id = sr.pid_pub
where r.l_egs = 0
  and r.type in ('complete')
  and vn.olang = 'ja'
  and r.patch = false
  and r.id in ( -- has japanese
    select releases_lang.id
    from releases_lang
    where releases_lang.lang = 'ja'
  )
--  and (r.released, sr.vid, sr.platform) in ( -- first release on its vn by platform
--    select min(r.released), sr.vid, sr.platform
--    from simple_release sr
--    inner join releases r on r.id = sr.id
--    where r.type = 'complete'
--      and r.patch = false
--      and r.id in ( -- has japanese
--        select releases_lang.id
--        from releases_lang
--        where releases_lang.lang = 'ja'
--      )
--    group by sr.vid, sr.platform
--  );
```

### VNDB releases with EGS (query copy)
https://query.vndb.org/queries/5f6f5fd6-f54d-43c6-b2dc-5a651f65c0fe
```
select id, l_egs
from releases
where l_egs != 0;
```
