#!/bin/env python

import json
import lxml
import lxml.etree
import cssselect

def main():
    with open("../../data/SQL実行フォーム -エロゲーマーのためのSQL-.html") as f:
        lines = f.readlines()
        lines = get_table_body_lines(lines)
        xml = "".join(lines)
        tbody = lxml.etree.fromstring(xml)
        releases = [
            create_release_dict(tr)
            for tr in tbody[1:]
        ]
    with open("../../data/egs_releases.json", "w+", encoding="utf-8") as f:
        json.dump(releases, f, indent=4, ensure_ascii=False)

def get_table_body_lines(lines):
    start = 0
    for line in lines:
        start += 1
        if line.strip() == '<div id="query_result_main"><table>':
            break
    table_body_lines = []
    for line in lines[start:]:
        if line.strip() == '</tbody></table>':
            table_body_lines.append('</tbody>')
            break
        table_body_lines.append(line)
    return table_body_lines

def create_release_dict(tr):
    tds = [td for td in tr]
    egs_id = int(tds[0].text)
    title = tds[1].text
    release_date = tds[2].text
    platform = tds[3].text
    if '(DOS)' in title and platform == "PC":
        platform = "DOS"
    producer_names = [
        name for name in tds[4].text.replace(')', '').split('(')
    ]
    return {
        'id': egs_id,
        'title': title,
        'release_date': release_date,
        'platform': platform,
        'producer_names': producer_names,
    }

if __name__ == '__main__':
    main()
