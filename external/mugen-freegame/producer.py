#!/bin/env python

import requests
import bs4
import time
import json

def main():
    producers = {}
    with open("../../src/output.txt") as f:
        for line in f:
            parts = line.split('\t')
            mugen_id = parts[0]
            producer = ""
            try:
                producers[mugen_id] = get_producer_names(mugen_id)
            except Exception as e:
                print(e)
            finally:
                time.sleep(1)
    with open("../../data/mugen_freegame.json") as f:
        games = json.load(f)
    kept_games = []
    for game in games:
        mugen_id = game['id']
        if mugen_id not in producers:
            continue
        game['producer_names'].extend(producers[mugen_id])
        kept_games.append(game)
    filename = "../../data/mugen_freegame2.json"
    with open(filename, "w+", encoding="utf-8") as f:
        json.dump(kept_games, f, indent=4, ensure_ascii=False)

def get_producer_names(mugen_id):
    print("Get page %s" % mugen_id)
    url = "https://freegame-mugen.jp/%s.html" % mugen_id
    response = requests.get(url, allow_redirects=False)
    response.encoding = response.apparent_encoding
    producer_names = set()
    soup = bs4.BeautifulSoup(response.text, "html.parser")
    ul = soup.find("ul", {
        'class': "postInfo",
    })
    producer_names.add(ul.find("li").findChildren("a")[0].string.strip())
    dl = soup.find("dl", {
        'class': "infoData",
    })
    zipped = zip(
        dl.findChildren("dt"),
        dl.findChildren("dd")
    )
    for dt, dd in zipped:
        key = dt.string.strip()
        if key == "制作者":
            value = dd.string.strip()
            producer_names.add(value)
        if key == "制作サイト":
            value = dd.find("a").string.strip()
            producer_names.add(value)
    return list(producer_names)

if __name__ == '__main__':
    main()
