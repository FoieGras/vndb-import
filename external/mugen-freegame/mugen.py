#!/bin/env python

import requests
import bs4
import time
import json

base_url = "https://freegame-mugen.jp/roleplaying/"

def main():
    games = []
    start = 1
    end = start + 39
    try:
        for i in range(start, end):
            print("Get page n°%d" % i)
            games.extend(get_page_games(i))
            time.sleep(1)
    except Exception as e:
        print(e)
    finally:
        filename = "../../data/eloge.json"
        with open(filename, "w+", encoding="utf-8") as f:
            json.dump(games, f, indent=4, ensure_ascii=False)

def get_page_games(i):
    url = "%s?page=%d" % (base_url, i)
    response = requests.get(url, allow_redirects=False)
    response.encoding = response.apparent_encoding
    soup = bs4.BeautifulSoup(response.text, "html.parser")
    ul = soup.find("ul", {
        'id': "catalog",
    })
    games = []
    for li in ul.findChildren("li"):
        url = li.find("a")["href"]
        mugen_id = url.lstrip('/').rstrip('.html')
        title = li.find("p", {
            'class': "gt",
        }).string
        if title is None:
            continue
        release_date = li.findChildren("span", {
            'class': "entrydate",
        })[0].string.replace('.', '-')
        games.append({
            "id": mugen_id,
            "title": title,
            "release_date": release_date,
            "platform": "PC",
            "producer_names": [],
        })
    return games

if __name__ == '__main__':
    main()
