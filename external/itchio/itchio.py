#!/bin/env python

import requests
import bs4
import time
import json

def main():
    games = []
    start = 341
    end = start + 471 - 340
    try:
        for i in range(start, end):
            print("Get page n°%d" % i)
            games.extend(get_page_games(i))
            time.sleep(1)
    except Exception as e:
        print(e)
    finally:
        filename = "../../data/itchio.json"
        with open(filename, "w+", encoding="utf-8") as f:
            json.dump(games, f, indent=4, ensure_ascii=False)

def get_page_games(i):
    url = "https://itch.io/games/genre-visual-novel?page=%d" % i
    response = requests.get(url, allow_redirects=False)
    soup = bs4.BeautifulSoup(response.text, "html.parser")
    main_div = soup.find("div", {
        'class': "browse_game_grid",
    })
    games = []
    for div in main_div.findChildren("div", {
            'class': "game_cell_data",
    }):
        title_a = div.find("a", {
            'class': "title"
        })
        url = title_a['href']
        title = title_a.string
        producer = div.find("div", {
            'class': "game_author",
        }).find("a").string
        games.append({
            "id": url,
            "title": title,
            "release_date": "2020-01-01",
            "platform": "PC",
            "producer_names": [producer],
        })
    return games

if __name__ == '__main__':
    main()
