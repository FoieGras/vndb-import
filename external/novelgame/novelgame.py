#!/bin/env python

import requests
import bs4
import time
import json

def main():
    games = []
    start = 0
    end = start + 115
    for i in range(start, end):
        print("Get page n°%d" % i)
        games.extend(get_page_games(i))
        time.sleep(1)
    filename = "../../data/novelgames.json"
    with open(filename, "w+", encoding="utf-8") as f:
        json.dump(games, f, indent=4, ensure_ascii=False)

def get_page_games(i):
    url = "https://novelgame.jp/games/index/page:%d" % i
    response = requests.get(url, allow_redirects=False)
    soup = bs4.BeautifulSoup(response.text, "html.parser")
    ul = soup.find("ul", {
        'class': "gameList",
    })
    games = []
    for li in ul.findChildren("li"):
        url = li.find("a")["href"]
        novelgame_id = url.split('/')[3]
        title = li.find("strong").string
        producer = li.find("span", {
            'class': "user",
        }).contents[2].strip()
        games.append({
            "id": novelgame_id,
            "title": title,
            "release_date": "2020-01-01",
            "platform": "PC",
            "producer_names": [producer],
        })
    return games

if __name__ == '__main__':
    main()
