#!/usr/bin/env python

# Copyright (c) 2021 Konosprod
# Some modifications by foiegras

import json
import time
import bs4
import requests

BASE_URL = "https://www.freem.ne.jp"

# 4 = novel game
# 3 = adv game
BASE_PAGE_URL = BASE_URL + "/win/category/3/page-"


def main():
    games = []
    start = 0
    end = start + 50
    for i in range(start, end):
        print("Get page n°%s" % i)
        src = get_source(BASE_PAGE_URL + str(i))
        games_urls = get_games(src)
        if games_urls is None:
            break
        for url in games_urls:
            print("Get game: " + url)
            src = get_source(url)
            game_dict = get_game_dict(src, url)
            games.append(game_dict)
    filename = "game_3_%d_%d.json" % (start, end)
    with open(filename, "w+", encoding="utf-8") as f:
        json.dump(games, f, indent=4, ensure_ascii=False)

def get_source(url):
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    r = requests.get(url, headers=headers, allow_redirects=False)
    time.sleep(1)
    return bs4.BeautifulSoup(r.text, "html.parser")

def get_games(src):
    listgame = src.find("ul", {"class":"game-list-wrap"})
    urls = None
    if listgame is not None:
        urls = []
        for game in listgame.findChildren("li", recursive=False):
            if "sp-ad-list" not in game.get("class"):
                urls.append(BASE_URL + game.find("a")["href"])
    return urls

def get_game_dict(src, url):
    freem_id = int(url.split('/')[-1])
    title = src.find("h1").getText().strip()
    details = src.find("table", {
        "class": "game-detail-table"
    })
    for detail in details.findChildren("tr", recursive=False):
        entry = detail.th.text.strip()
        if entry == "[OS]":
            os = detail.td.text.strip().lower()
            if "win" in os:
                platform = "PC"
            elif "browser" in os:
                platform = "WEB"
            else:
                platform = os
        if entry == "[Registered]":
            release_date = detail.td.text.strip()
    namediv = src.find("div", {
        "class": "game-creator-name"
    })
    producer_name = namediv.h3.a.string.strip()
    return {
        'id': freem_id,
        'title': title,
        'release_date': release_date,
        'platform': platform,
        'producer_names': [producer_name],
    }

if "__main__" == __name__:
    main()
