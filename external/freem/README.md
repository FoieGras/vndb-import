Adapted from https://github.com/Konosprod/freem-extract
Kudos to Konosprod.

The file `freem.py` in this directory is under the provided MIT license and sublicensed under the GPLv3 located at the root of this git repository.
