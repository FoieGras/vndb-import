#!/bin/env python

filename = "freegame_mugen_not_same_producer"

input_name = "../../src/%s.txt" % filename
output_name = "../../src/%s_output.txt" % filename
with open(input_name) as f:
    lines = [
        "\t".join(line.split("\t")[0:9])
        for line in f
    ]
with open(output_name, "w+") as f:
    for line in lines:
        f.write(line + "\n")
