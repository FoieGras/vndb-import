#!/bin/python

def main():
    with open('../../src/freegame_mugen_not_same_producer_output.txt') as out_file:
        check_tabs(out_file)
        out_file.seek(0)
        check_ids(out_file)
    print("ok")

def check_tabs(out_file):
    print("Checking tabs")
    expected_count = None
    for line in out_file:
        count = line.count('\t')
        if expected_count is None:
            expected_count = count
        elif count != expected_count:
            print('error on %s' % line)

def check_ids(out_file):
    print("Checking ids")
    external_ids = set()
    vndb_release_ids = set()
    for line in out_file:
        parts = line.split("\t")
        external_id = parts[0].strip()
        vndb_release_id = parts[2].strip()
        if external_id in external_ids:
            print("Double external %s" % external_id)
        if vndb_release_id in vndb_release_ids:
            print("Double vndb %s" % vndb_release_id)
        external_ids.add(external_id)
        vndb_release_ids.add(vndb_release_id)

if __name__ == '__main__':
    main()
