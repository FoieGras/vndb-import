#!/usr/bin/env python

import json

def main():
    filenames = [
        "itchio_1_341.json",
        "itchio_342_471.json",
    ]
    games = []
    for filename in filenames:
        with open(filename) as f:
            data = json.load(f)
            games.extend(data)
    with open("../../data/itchio.json", "w+", encoding="utf-8") as f:
        json.dump(games, f, indent=4, ensure_ascii=False)

if __name__ == '__main__':
    main()
